//handshake
//send "ready?" from PC
//send "ok." from Arduino

#define BAUDRATE 115200
#define HANDSHAKE_FROM_PC "ready?\r\n"
#define HANDSHAKE_FROM_ARDUINO "ok.\r\n"


struct PacketHeader{
  byte size;
  byte type;
}__attribute__((packed));

#define TYPE_RE_ARRAY01 1
#define TYPE_RE_ARRAY07 7
#define TYPE_SW_ARRAY02 16+2

struct PacketReArray01{
  PacketHeader header;
  byte reArray[1];
}__attribute__((packed));

struct PacketReArray07{
  PacketHeader header;
  byte reArray[7];
}__attribute__((packed));

struct PacketSwArray02{
  PacketHeader header;
  byte swArray[2];
}__attribute__((packed));
