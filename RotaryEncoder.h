#pragma once

//ec11MHBFZ-B-15K-15-30C-16Y
//360度=30クリック=15パルス(Hz)
//1クリックごとに00と11の状態を繰り返す
//中間状態を挟まない時は推測で値を進める
//素早く回転させた時、中間状態を2[ms]で通過したため、実行間隔に注意
class RotaryEncoder{
  int mPinA;//0x2
  int mPinB;//0x1
  int mBits;//mPinA<<1 | mPinB
  int mDirection;//normal:1,flip:-1
  int mPosition;
  int mVelocity;//-1,0,+1
  public:
  RotaryEncoder(int inPinA,int inPinB,int inDirection=1)
  :mPinA(inPinA)
  ,mPinB(inPinB)
  ,mBits(0)
  ,mDirection(inDirection)
  ,mPosition(0)
  ,mVelocity(0){
    pinMode(mPinA,INPUT_PULLUP);
    pinMode(mPinB,INPUT_PULLUP);
  }
  void update(){
    int a=digitalRead(mPinA);
    int b=digitalRead(mPinB);
    int currentBits=(a==HIGH?2:0)|(b==HIGH?1:0);
    if(mBits!=currentBits){
      if(
        (mBits==0&&currentBits==1)||
        (mBits==1&&currentBits==3)||
        (mBits==3&&currentBits==2)||
        (mBits==2&&currentBits==0)
      ){
        //CW
        mVelocity=1*mDirection;
      }else if(
        (mBits==1&&currentBits==0)||
        (mBits==3&&currentBits==1)||
        (mBits==2&&currentBits==3)||
        (mBits==0&&currentBits==2)
      ){
        //CCW
        mVelocity=-1*mDirection;
      }else{
        //脱調時、前回値を参考に進める
        //mBits^currentBits==3 を想定
        
        if(mVelocity<0){
          mVelocity=-2*mDirection;
        }else if(0<mVelocity){
          mVelocity=2*mDirection;
        }else{
          //DO NOTHING
        }
      }
    }else{
      mVelocity=0;
    }
    mPosition+=mVelocity;
    mBits=currentBits;
  }
  int getBits()const{
    return mBits;
  }
  int getPosition()const{
    return mPosition;
  }
  int getVelocity()const{
    return mVelocity;
  }
};
