


#include "packet.h"
#include "RotaryEncoder.h"

const unsigned long PACKET_INTERVAL_US=(unsigned long)(1000.0*1000.0/120.0);
const unsigned long PACKET_LONG_DELAY_US=(unsigned long)(1000.0*1000.0/120.0);
const unsigned long POLLING_LONG_DELAY_US=(unsigned long)(1000.0*1000.0/1000.0);

#define ROTARY_ENCODER_1_A 2
#define ROTARY_ENCODER_1_B 3
#define ROTARY_ENCODER_2_A 4
#define ROTARY_ENCODER_2_B 5
#define ROTARY_ENCODER_3_A 6
#define ROTARY_ENCODER_3_B 7
#define ROTARY_ENCODER_4_A 8
#define ROTARY_ENCODER_4_B 9
#define ROTARY_ENCODER_5_A A0
#define ROTARY_ENCODER_5_B A1
#define ROTARY_ENCODER_6_A A2
#define ROTARY_ENCODER_6_B A3
#define ROTARY_ENCODER_7_A A4
#define ROTARY_ENCODER_7_B A5

#define POWER_TOGGLE 10
#define DEBUG_TOGGLE 11

#define ALERT_PIN 13

//3*2+4*2+2+2
//アナルグピン側RE 3*2
//デジタルピン側RE 4*2

//トグルスイッチ 2
//5V+GND 2


PacketReArray07 packetReArray07;
#define RE_QTY 7
RotaryEncoder rotaryEncoderArray[RE_QTY]={
  RotaryEncoder(ROTARY_ENCODER_1_A,ROTARY_ENCODER_1_B,-1),
  RotaryEncoder(ROTARY_ENCODER_2_A,ROTARY_ENCODER_2_B,-1),
  RotaryEncoder(ROTARY_ENCODER_3_A,ROTARY_ENCODER_3_B,-1),
  RotaryEncoder(ROTARY_ENCODER_4_A,ROTARY_ENCODER_4_B,-1),
  RotaryEncoder(ROTARY_ENCODER_5_A,ROTARY_ENCODER_5_B,-1),
  RotaryEncoder(ROTARY_ENCODER_6_A,ROTARY_ENCODER_6_B,-1),
  RotaryEncoder(ROTARY_ENCODER_7_A,ROTARY_ENCODER_7_B,-1),
};
PacketSwArray02 packetSwArray02;
#define SW_QTY 2
int swPins[SW_QTY]={POWER_TOGGLE,DEBUG_TOGGLE};

unsigned long timeMicrosPrevPacket=0;
unsigned long timeMicrosPrevPolling=0;

enum HandshakeState{
  HandshakeState_Initializing,
  HandshakeState_Ready,
}handshakeState=HandshakeState_Initializing;
void setup() {
  pinMode(ALERT_PIN, OUTPUT);
  pinMode(POWER_TOGGLE,INPUT_PULLUP);
  pinMode(DEBUG_TOGGLE,INPUT_PULLUP);

  memset(&packetReArray07,0,sizeof(packetReArray07));
  packetReArray07.header.size=sizeof(packetReArray07);
  packetReArray07.header.type=TYPE_RE_ARRAY07;

  memset(&packetSwArray02,0,sizeof(packetSwArray02));
  packetSwArray02.header.size=sizeof(packetSwArray02);
  packetSwArray02.header.type=TYPE_SW_ARRAY02;

  Serial.begin(BAUDRATE);
  
  //Serial.print(sizeof(packet));
}

void loopInitializing(){
  if(0<Serial.available()){
    String str=Serial.readStringUntil('\n');
    str+='\n';
    if(str==HANDSHAKE_FROM_PC){
      Serial.print(HANDSHAKE_FROM_ARDUINO);
      handshakeState=HandshakeState_Ready;
      timeMicrosPrevPacket=micros();
      timeMicrosPrevPolling=micros();
    }
  }
}


void loopReady(){
  unsigned long timeMicros=micros();
  unsigned long pollingInterval=timeMicros-timeMicrosPrevPolling;
  if(pollingInterval>=POLLING_LONG_DELAY_US){
    digitalWrite(ALERT_PIN, HIGH);
  }else{
    //digitalWrite(ALERT_PIN, LOW);
  }
  
  for(int i=0;i<RE_QTY;++i){
    RotaryEncoder& rotaryEncoder=rotaryEncoderArray[i];
    byte& re=packetReArray07.reArray[i];
    rotaryEncoder.update();
    //int bits=rotaryEncoder.getBits();
    int position=rotaryEncoder.getPosition();
    //int velocity=rotaryEncoder.getVelocity();
    //Serial.println(position);
    re=(byte)position;
  }
  for(int i=0;i<SW_QTY;++i){
    byte& sw=packetSwArray02.swArray[i];
    int swPin=swPins[i];
    sw=(byte)(1-digitalRead(swPin));
  }
  
  timeMicrosPrevPolling=timeMicros;
  
  unsigned long packetInterval=timeMicros-timeMicrosPrevPacket;
  if(packetInterval>=PACKET_INTERVAL_US){
    if(packetInterval>=PACKET_INTERVAL_US+PACKET_LONG_DELAY_US){
      digitalWrite(ALERT_PIN, HIGH);
    }else{
      //digitalWrite(ALERT_PIN, LOW);
    }
    Serial.write((uint8_t*)&packetReArray07,sizeof(packetReArray07));
    Serial.write((uint8_t*)&packetSwArray02,sizeof(packetSwArray02));
    
        
    timeMicrosPrevPacket=timeMicros;
  }
}

void loop() {
  /*
  if(Serial.available()){
    int data=Serial.read();
    if(data!=-1){
      Serial.write((byte)data);
    }
  }
  
  return;
  */
  switch(handshakeState){
    case HandshakeState_Initializing:
    loopInitializing();
    break;
    case HandshakeState_Ready:
    loopReady();
    break;
  }
  
}
